const express = require('express');

// Lodash utils library
const _ = require('lodash');

const router = express.Router();

// Utile pour Axios
const axios = require('axios');
const api_url = "http://www.omdbapi.com/";
const api_key = "52980ec7";

/*
// Create RAW data array
let movies = [{
  movie: "LaLaLand",
  id: "0"
}];
*/

let movies = [];

/* GET movies listing. */
router.get('/', (req, res) => {
  // Get List of movie and return JSON
  res.status(200).json({ movies });
});

/* GET un film via son ID */
router.get('/:id', (req, res) => {
  const { id } = req.params;
  // Find movie in DB
  const movie = _.find(movies, ["id", id]);
  // Return user
  res.status(200).json({
    message: 'Film found!',
    movie 
  });
});


/* PUT new movie via son Title
router.put('/:title', (req, res) => {
  //Récupérer la data de la requête
  const { movie } = req.body;
  // Création d'un nouvel unique ID
  const id = _.uniqueId();
  // Ajout au tableau movies
  movies.push({ movie, id });
  // Return message
  res.json({
    message: `Ajout de ${id}`,
    movie: { movie, id }
  });
});
*/

/* PUT nouveau movie via son Title */

router.put('/:title', (req, res) => {
  axios.get(`${api_url}?t=${req.params.title}&apikey=${api_key}`).then(function (response) 
  {
    // response.data : sert à récupérer data de l'API
    const movie = {
      id: response.data.imdbID, // String
      movie: response.data.Title, // String
      yearOfRelease: response.data.Year, //Number
      duration: response.data.Runtime,// Number : en minutes
      actors: response.data.Actors, // [String, String]
      poster: response.data.Poster, // String : lien vers une image d'affiche
      boxOffice: response.data.BoxOffice, // Number : en USD$
      rottenTomatoesScore: response.data.imdbRating //Number

    };
   
    // Ajout au tableau movies
    movies.push(movie);

    res.status(200).json({
      movies
      /*: [
          {
              message: `Le film ${movie} ajouté !`,
              id : `id : ${id}`,
              yearOfRelease: yearOfRelease,
              duration: duration,
              actors: actors,
              poster: poster,
              boxOffice: boxOffice,
              rottenTomatoesScore: rottenTomatoesScore
          }]*/
    })

  })
  .catch(function (error) {
    // handle error
    console.log(error);
  })
  .finally(function () {
    // always executed
  });
});


/* DELETE movie  via son ID */
router.delete('/:id', (req, res) => {
  // Get the :id of the user we want to delete from the params of the request
  const { id } = req.params;

  // Remove from "DB"
  _.remove(movies, ["id", id]);

  // Return message
  res.json({
    message: `Just removed ${id}`
  });
});


/* UPDATE(POST) movie via son ID. */
router.post('/:id', (req, res) => {
  // Récupérer :id du film qu'on souhaite update from the params of the request
  const { id } = req.params;
  // Récupérer la new data du film qu'on veut update from the body of the request
  const newNomMovie = req.body.movie;
  console.log(req.body);
  // Find in DB
  const userToUpdate = _.find(movies, ["id", id]);
  // Update data with new data (js is by address)
  userToUpdate.movie = newNomMovie ;

  // Return message
  res.json({
    message: `Just updated ${id} with ${newNomMovie}`
  });
});

module.exports = router;
